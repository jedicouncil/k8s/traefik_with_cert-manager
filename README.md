# Traefik configuration on k3s secured with cert-manager

Inspired from 2 blogs :
* [ovh](https://www.cerenit.fr/blog/kubernetes-ovh-traefik2-cert-manager-secrets/)
* [djerfy](https://xorhak.io/kubernetes-ingress-controller-traefik-v2-1/)

Create namespaces first 

```bash
kubectl create ns traefik2
kubectl create ns cert-manager
```

**More info & details soon...**